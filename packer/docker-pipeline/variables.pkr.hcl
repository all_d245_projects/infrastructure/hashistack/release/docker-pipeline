#> common
variable "docker_login_server" {
  type        = string
  description = "Docker registry server"
  default     = "registry.gitlab.com"
}
variable "docker_login_username" {
  type        = string
  description = "Docker registry login username"
  default     = "hashistack-docker-registry"
}
variable "docker_login_password" {
  type        = string
  sensitive   = true
  description = "Docker registry login password"
}
variable "packer_version" {
  type        = string
  description = "Packer version to install in docker"
  default     = "1.10.0"
}
variable "ansible_version" {
  type        = string
  description = "Ansible version to install in docker"
  default     = "9.1.0"
}
variable "docker_tag_repository" {
  type        = string
  description = "Docker tag repository"
  default     = "registry.gitlab.com/all_d245_projects/infrastructure/hashistack/release/docker-pipeline"
}
variable "docker_tag_tags" {
  type        = set(string)
  description = "Docker tag array of tags"
  default     = ["latest"]
}
#<